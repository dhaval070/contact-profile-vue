import Vue from 'vue';
import Router from 'vue-router';
import CreateProfile from './components/CreateProfile';
import ListProfile from './components/ListProfile';
import EditProfile from './components/EditProfile';
import ViewProfile from './components/ViewProfile';

Vue.use(Router);

export default new Router({
	routes: [
	{
		path: '/',
		name: 'list-profile',
		component: ListProfile
    },
    {
		path: '/profiles',
		name: 'list-profiles',
		component: ListProfile
    },
    {
        path: '/create',
        name: 'create-profile',
        component: CreateProfile
    },
    {
        path: '/edit/:id',
        name: 'edit-profile',
        component: EditProfile
    },
    {
        path: '/view/:id',
        name: 'view-profile',
        component: ViewProfile
    }
  ]
})