// vue.config.js
module.exports = {
  // options...
  devServer: {
        proxy: 'http://localhost:8000/'
    },
    publicPath: process.env.NODE_ENV == 'production' ? '/contact-profile/' : '/'
}
